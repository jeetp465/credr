import os
import re
import string

input_file = open('/home/jeet/Documents/CredR/OCR_Text_Extraction/output.txt','r')
lines = input_file.readlines()

output_file = open('/home/jeet/Documents/CredR/OCR_Text_Extraction/cleantext.txt','w')

def get_reg_date(lines) :
	a = re.compile("REG")
	i = 0
	for line in lines:
		line = line.replace(' I ',' : ')
		line = line.replace(' ; ',' : ')
		if a.match(line) :
			print line
		 	if i == 1:
				line = line.split(': ')
				output_file.write("REG. DT: ")
				count = 0
				for letter in line[1]:
					if count == 10:
						break
					if count == 2:
						letter = '/'
					elif count == 5:
						letter = '/'
					output_file.write(letter)
					count = count + 1
				output_file.write("\n")
			i = i+1

def get_mfg_date(lines) :
	a = re.compile("MFG")
	b = 'II'

	for line in lines:
		line = line.replace(' I ',' : ')	
		if a.match(line):
			output_file.write('MFG DT: ')
			line = line.split(': ')
			count = 0
			for letter in line[1]:
				if count == 7:
					break
				elif count ==2:
					letter = '/'	
				output_file.write(letter)
				count = count + 1
			output_file.write("\n")
		elif b in line :
			print "yesS"
			line = line.replace('II','M')
			if a.match(line):
				output_file.write('MFG DT: ')
				line = line.split(': ')
				count = 0
				for letter in line[1]:
					if count == 7:
						break
					elif count ==2:
						letter = '/'	
					output_file.write(letter)
					count = count + 1
				output_file.write("\n")

def get_engine_no(lines):
	a = re.compile("ENG")
	b = re.compile("E NO")
	for line in lines:
		line = line.replace(' I ',' : ')
		if a.match(line) or b.match(line):
			print line
			output_file.write('ENGINE NO: ')
			line = line.split(": ")
			for letter in line[1]:
				if letter == ' ':
					break
				else:
					output_file.write(letter)
			output_file.write("\n")

def get_color(lines):
	a = re.compile('COL')
	for line in lines:
		line = line.replace(' I ',' : ')
		line = line.replace(' 2 ',' : ')
		line = line.replace(' 1 ',' : ')
		if a.match(line):
			print line
			output_file.write('COLOR : ')
			line = line.split(": ")
			output_file.write(line[1])
			output_file.write("\n")
		elif 'COLOUR' in line:
			print line
			output_file.write('COLOR : ')
			line = line.split(": ")
			print line
			output_file.write(line[2])
			output_file.write("\n")

def get_chassis_no(lines):
	a = re.compile('CH')
	for line in lines:
		line = line.replace(' I ',' : ')
		if a.match(line):
			print line
			output_file.write("CHASSIS NO: ")
			line = line.split(': ')
			for letter in line[1]:
				if letter == ' ':
					break
				else:
					output_file.write(letter)
			output_file.write("\n")

def get_osl_no(lines):
	#a = re.compile('O.SL')
	for line in lines:
		line = line.replace(' I ',' : ')
		if 'O.SL' in line:
			print line
			line = line.split(': ')
			output_file.write('O.SL.NO : ')
			output_file.write(line[2])
			output_file.write('\n')

get_chassis_no(lines)