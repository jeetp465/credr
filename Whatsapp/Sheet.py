"""
This script uses selenium. Preferably run this when 'send_data.py' is not running. If other PC is avaiable then run it there.
To run this script using Terminal type : sudo python Sheet.py
"""
import gspread
import os
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd 
import psycopg2
from selenium import webdriver
import traceback
import unittest

conn = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!", host="52.74.154.2", port="5432")
# conn = psycopg2.connect(database="whatsapp", user="whatsapp_admin", password="wh@t^a**d!", host="52.76.193.246", port="5432")
cur = conn.cursor()
print "Connection to Database Established"

########################################################################################################################
#########################################    READING DATA FROM GOOGLE SHEET   ##########################################
########################################################################################################################

# defining credentials to connect to google sheet python api (Credentials can be obtained at google API cpnsole)
# currently credentials of google API console are obtained using whatsapp@credr.com 

os.chdir('/home/jeet/Downloads')
scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('Whatsapp C2B-3d76016fd6da.json', scope)

gc = gspread.authorize(credentials)
print "Connection to Google Sheets API Established"

# url of google sheet where contact list is maintained. If in future the goole sheet is updated then share it to the client
# email provided in 'Whatsapp C2B-3d76016fd6da.json'.
sh = gc.open_by_url('https://docs.google.com/spreadsheets/d/1UtTm0M5tqQq6W1NvFsKhPmsu4YLIwfUR2zlvKiOvYRY/edit#gid=0')

worksheet_list = sh.worksheets()
# print worksheet_list
worksheet = worksheet_list[0]
print worksheet
data = worksheet.get_all_values()        # list of rows (where each row is a list of cells)
headers = data[0]
data = data[1:]
global df
df = pd.DataFrame(data, columns =headers)
global df1

# '2': Added in Database, '3': Google contact Added, '4': User is registered	:	df['Status']
# insert into database if new_user is 1 and update it to 0						:	df['new_user']
# insert into group when group_add_status is 1 and then after adding update it to 0(updation to be done manually)	:	df['group addition']

########################################################################################################################
########################################    POST PROCESSING DATA FROM SHEETS   #########################################
########################################################################################################################

# check if there is a new user. If present add it to the database.
if len(df[df['new_user'] == '1']):
	global df1
	df1 = df[df['new_user'] > 0]
	status = 0
	city = df['City']
	dealer_name = df1['Shop_Name']
	Dealer_id = df1['Dealer_id']
	whatsapp_no = df1['whatsapp_no']
	outlet_id = df1['Outlet_Id']
	try:
		for i in range(len(df1)):
			insert_user = "Insert into c2b_users (user_id,city,phone_no,status,shop_name,outlet_id) values ("+str(Dealer_id[i])+",'"+str(city[i])+"','"+str(whatsapp_no[i])+"',"+str(status)+",'"+str(dealer_name[i])+"','"+str(outlet_id[i])+"');" 
			cur.execute(insert_user)
			location = df.index.get_indexer_for(df[df['Shop_Name']== dealer_name[i]].index)
			print location[0], type(location[0])
			worksheet.update_cell(int(location[0]+2),8,0)
			worksheet.update_cell(int(location[0]+2),5,2)
	except :
	    conn.rollback()
	    traceback.print_exc()

	# conn.commit()
	print "Users inserted"

# if the user is inserted in the database then upload it to google contacts.
print len(df[df['Status'] == '2'])
	global df1
	df1 = df[df['Status'] == '2']
	
	class ContactUpload(unittest.TestCase):

	    def setUp(self):
	        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
	        self.driver = webdriver.Firefox(self.profile)
	        self.driver.implicitly_wait(30)
	        self.base_url = "https://mail.google.com/"
	        self.verificationErrors = []
	        self.accept_next_alert = True
	    
	    def test_contact_upload(self):
	        flag = 0
	        global df1
	        contact_no = df1['whatsapp_no']
	        Shop_Name = df1['Shop_Name']
	        driver = self.driver
	        driver.get(self.base_url + "/mail/u/2/#contact/new")
	        
	        for i in range(len(Shop_Name)):
	            shop_name = Shop_Name[i]
	            contact = contact_no[i]    
	            #returns an array of the location where the element is located. To access the element we use location[0]
	            location = df.index.get_indexer_for(df[df['Shop_Name']== Shop_Name[i]].index)		

	            if flag == 0:
	                driver.find_element_by_xpath("//input[@class='R5 Rh']").clear()
	                driver.find_element_by_xpath("//input[@class='R5 Rh']").send_keys(str(shop_name))
	                flag = 1
	            elif flag == 1:
	                driver.find_element_by_css_selector("span.acm > input.R5.Rh").clear()
	                driver.find_element_by_css_selector("span.acm > input.R5.Rh").send_keys(str(shop_name))
	            time.sleep(1)   
	            for j in range(0,9):
	                pyautogui.press('tab')
	                time.sleep(0.45)
	            time.sleep(1.25)
	            pyautogui.typewrite(str(contact))
	            time.sleep(1.25)
	            driver.find_element_by_xpath("//div[@class='MX']/child::div").click()
	            time.sleep(1.25)

	            worksheet.update_cell(int(location+1),5,3)

	    def tearDown(self):
	        self.driver.quit()
	        self.assertEqual([], self.verificationErrors)

	if __name__ == "__main__":
	    unittest.main()
	    print "Contacts Uploaded"

