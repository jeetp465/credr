# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, ast
import psycopg2
import json
import pyautogui
import os
import urllib
import webbrowser
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from lxml import html
import lxml
import traceback
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from table_dict import c2b_sender,c2b_bikes,c2b_receiver


########################################################################################################################
#########################################    READING DATA FROM GOOGLE SHEET   ##########################################
########################################################################################################################

scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('Whatsapp C2B-3d76016fd6da.json', scope)

gc = gspread.authorize(credentials)
print "Connection to Google Sheets API Established"

sh = gc.open_by_url('https://docs.google.com/spreadsheets/d/1UtTm0M5tqQq6W1NvFsKhPmsu4YLIwfUR2zlvKiOvYRY/edit#gid=0')

worksheet_list = sh.worksheets()
worksheet = worksheet_list[0]

data = worksheet.get_all_values()        # list of rows (where each row is a list of cells)
headers = data[0]
global df
df = pd.DataFrame(data, columns=headers)

########################################################################################################################
###############################################    CONNECTING TO DATABASE    ###########################################
########################################################################################################################

# conn = psycopg2.connect(database="whatsapp", user="whatsapp_admin", password="wh@t^a**d!", host="52.76.193.246", port="5432")
conn = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!", host="52.74.154.2", port="5432")
cur = conn.cursor()
conn.commit()
print "Connection Established"
present_contact =''

########################################################################################################################
################################################    UPDATING SENDERS TABLE    ##########################################
########################################################################################################################

def update(num):

    query = "update c2b_sender SET sms_status = 1 WHERE sr_no = '" + str(num) + "';"
    cur.execute(query)  
    conn.commit()

########################################################################################################################
###################################################    CHECK NEXT CITY    ##############################################
########################################################################################################################

def check_next(num,city):

    query = "select * from c2b_sender where sr_no = '" + str(num + 1) + "';"
    cur.execute(query)
    row = cur.fetchone()

    if row != None:
        next_city = row[int(c2b_sender.get("shop_name"))]
        
        if str(next_city) == str(city):
            print "Next Bike is of same city"
            time.sleep(60)
        else:
            pass

########################################################################################################################
#################################################   DOWNLOADING IMAGE   ################################################
########################################################################################################################

global count
count = 1
def download_image(image_source):

    try:
        image_source = str(image_source)
        print image_source
        print "image Downloading..."
        urllib.urlretrieve(image_source,"/home/jeet/CredR/Whatsapp/images/upload.jpg")
        print "image Downloaded"
        time.sleep(5)
    except:
        global count
        if count <=3:
            download_image(image_source)
            count +=1
        else:
            print "Internet connection is slow."
########################################################################################################################
################################################  UPDATE BID TIME START  ###############################################
########################################################################################################################

def update_bike_sms_timestamp(bike_id):

    query = "update c2b_bikes set sms_send_time = now() where bike_id = '"+str(bike_id)+"';"
    cur.execute(query)
    conn.commit()

########################################################################################################################
################################################    TYPEWRITE WITH DELAY   #############################################
########################################################################################################################

def type_write_delay(text):
    for i in text:
        pyautogui.typewrite(i)
        time.sleep(0.05)

########################################################################################################################
##################################################    RUNNING UNITTEST   ###############################################
########################################################################################################################

class New(unittest.TestCase):

    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def open_me(self):
        self.open_contact('Jeet CredR')

    def open_contact(self,contact):
        global present_contact
        if(present_contact != contact):
            self.driver.find_element_by_xpath("//input[@title='Search or start new chat']").click()
            type_write_delay(contact)
            time.sleep(1)
            self.driver.find_element_by_xpath("//span[@title='" + contact + "']").click()
            present_contact = contact

    def total_unread_proper_msgs(self):
        try:
            elms = self.driver.find_elements_by_xpath("//*[@class='unread chat']")
        except:
            elms = []
            pass
        contacts =[]
        for elm in elms:
            text = elm.text
            words = text.splitlines()
            if "+91" in words[0]:
                continue
            contacts.append(words[3])
        return contacts

    def receiver(self):
        global present_contact
        try:
            elms = self.driver.find_elements_by_xpath("//*[@class='unread chat']")
        except:
            elms = []
            pass

        for elm in elms:
            text = elm.text
            words = text.splitlines()
            if "+91" in words[0]:
                continue
            present_contact = words[0]
            elm.click()
            
            try:
                check_group = self.driver.find_element_by_xpath("/span[@class='emojitext ellipisify']").text#get_attribute("title")
                print check_group
            except:
                print "dsajkl"
            
            try:
                msgs = self.driver.find_elements_by_xpath("//div[contains(@class, 'message-in')]")
                print " in receiver"
                print len(msgs)
                if len(msgs[-1 * int(words[3]):]) > 0:
                    for msg in msgs[-1 * int(words[3]):]:
                        message_list = msg.text.splitlines()
                        print "message list ",message_list, words[0]
                        if len(message_list) == 2:
                            message_list[1] = message_list[1].encode('ascii', 'ignore')
                            message_list[0] = message_list[0].encode('ascii', 'ignore')

                            shop_name = str(words[0])
                            msg = str(message_list[0])
                            data = (msg, shop_name, '0')

                            print "from: " + shop_name + " == " + msg + " at " + message_list[1]
                            query = "INSERT INTO c2b_receiver (msg, from_name, status) VALUES (%s,%s,%s)"
                            cur.execute(query, data)
                            conn.commit()

            except IndexError:
                print "Received emoji"

            time.sleep(1)

    def sender(self):
        query = "Select * from c2b_sender where sms_status = 0 order by sr_no asc"
        cur.execute(query)

        rows = cur.fetchall()

        if len(rows):
            for row in rows:
                sending_to = row[int(c2b_sender.get("shop_name"))]
                new_msg_receiving_contacts = self.total_unread_proper_msgs()
                if len(new_msg_receiving_contacts):
                    self.receiver()
                print row[int(c2b_sender.get("message"))]
                data = ast.literal_eval(str(row[int(c2b_sender.get("message"))]).replace('\\', ''))
                print data
                self.open_contact(sending_to)
                if 'text' in data.keys():
                    self.driver.find_element_by_xpath("//*[@id='main']/footer/div/div[1]/div/div[2]").send_keys(data['text'])
                    self.driver.find_element_by_xpath("//div[@id='main']/footer/div/button[2]").click()
                elif 'image' in data.keys():
                    download_image(data['image'])
                    self.driver.find_element_by_css_selector("button.icon.icon-clip").click()
                    time.sleep(0.5)
                    self.driver.find_element_by_css_selector("button.menu-icons-item").click()
                    time.sleep(0.5)
                    self.driver.find_element_by_xpath("(//input[@type='file'])[2]")
                    time.sleep(1)
                    pyautogui.press('enter')
                    time.sleep(1)
                    
                    if 'caption' in data.keys():
                        type_write_delay(data['caption'])
                    time.sleep(0.5)
                    self.driver.find_element_by_xpath(
                        "//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
                    
                    if 'bike_id' in data.keys():
                        update_bike_sms_timestamp(data['bike_id'])
                        check_next(row[int(c2b_sender.get('sr_no'))],row[int(c2b_sender.get('shop_name'))])
                
                update(row[int(c2b_sender.get('sr_no'))])

    def refresh(self,city):
        global present_contact
        driver = self.driver
        driver.find_element_by_xpath("//span[@title='"+ str(city) +"']").click()
        time.sleep(1)
        driver.find_element_by_css_selector("h2.chat-title > span.emojitext.ellipsify").click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//div[@class='drawer-section-body drawer-section-body-expand chatlist']")
        
        html_info = element.get_attribute('innerHTML')
        info=lxml.html.fromstring(html_info)
        
        text = info.xpath("//div//div[@class='chat-secondary']")
        for status in text:
            status.getparent().remove(status)
        
        result=info.xpath("//div//span[@class='emojitext ellipsify']")
        
        present_contact = city
        data = []
        for element in result:
            element = lxml.html.tostring(element)
            element = element.split('>')
            element = element[1].split('<')
            data.append(element[0])
        temp =[]
        query = "Select shop_name from c2b_users where status = 1 and city ='" + str(city) + "';"
        cur.execute(query)
        all_data = cur.fetchall()
        
        for item in all_data:
            temp.append(item[0])    

        refresh_data = [x for x in temp if x not in data]
        final_data = ', '.join(x for x in refresh_data)

        global df
        for data in final_data:
            location = df.index.get_indexer_for(df[df['Shop_Name']== data].index)
            # print location[0]
            worksheet.update_cell(int(location[0]+2),9,1)


    def test_new(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        time.sleep(30)
        self.driver.implicitly_wait(1)
        self.open_me()
        
        while(1):
            try:
                self.receiver()
                self.open_me()
                self.sender()
                self.open_me()

                query = "Select * from c2b_receiver where status = 12"
                cur.execute(query)
                rows = cur.fetchall()

                if len(rows):
                    for row in rows:
                        msg = row[int(c2b_receiver.get('msg'))]
                        msg = msg.split('-')
                        city = msg[1] 
                        self.refresh(city)
                        upt_query = "Update c2b_receiver set status = 8 where msg_id = " + str(row[int(c2b_receiver.get('msg_id'))])
                        cur.execute(upt_query)
                        conn.commit()
            except:
                continue

    def sendmail(self):
        fromaddr = "whatsapp@credr.com"
        toaddr = "jeet.patel@credr.com"
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "Code Crashed!"
         
        body = "Woah we're hosed!! send_data.py has crashed. Restart immediately !!"
        msg.attach(MIMEText(body, 'plain'))
         
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, "whatsapp_c2b")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()

    def tearDown(self):
        self.sendmail()
        time.sleep(3)
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
