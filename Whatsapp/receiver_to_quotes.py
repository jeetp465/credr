import psycopg2,  requests,time
import traceback
import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import os

from table_dict import c2b_receiver
conn = psycopg2.connect(database="whatsapp", user="whatsapp_admin", password="wh@t^a**d!", host="52.76.193.246", port="5432")
# conn = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!", host="52.74.154.2", port="5432")
cur = conn.cursor()

count = 0

import smtplib

########################################################################################################################
#########################################    READING DATA FROM GOOGLE SHEET   ##########################################
########################################################################################################################

scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('Whatsapp C2B-3d76016fd6da.json', scope)

gc = gspread.authorize(credentials)
print "Connection to Google Sheets API Established"

sh = gc.open_by_url('https://docs.google.com/spreadsheets/d/1UtTm0M5tqQq6W1NvFsKhPmsu4YLIwfUR2zlvKiOvYRY/edit#gid=0')

worksheet_list = sh.worksheets()
worksheet = worksheet_list[0]

data = worksheet.get_all_values()        # list of rows (where each row is a list of cells)
headers = data[0]
global df
df = pd.DataFrame(data, columns=headers)

########################################################################################################################
############################################    GENERATING RESPONSE MESSAGES   #########################################
########################################################################################################################

while (1):
	try:
		msgs_query = "SELECT * from c2b_receiver where status = '0';"
		cur.execute(msgs_query)
		replies = cur.fetchall()
		print replies

		for reply in replies:
			msg_id = reply[int(c2b_receiver.get('msg_id'))]
			msg = reply[int(c2b_receiver.get('msg'))]
			time = reply[int(c2b_receiver.get('time'))]
			shop_name = reply[int(c2b_receiver.get('shop_name'))]

			# Check if the mesage is to refresh broadcast list
			if shop_name == 'Jeet CredR':
				refresh_query = "UPDATE c2b_receiver set status ='12' where msg_id='" + str(msg_id) + "';"
				cur.execute(refresh_query)
				conn.commit()

				continue

			# Verify user
			if msg.lower() == 'hi':
				#Checking if user is in our db and previously not registered
				user_query = "SELECT user_id from c2b_users where shop_name = '" + shop_name + "' and status = '0';"
				cur.execute(user_query)
				x = cur.fetchone()
				user_query = "UPDATE c2b_users set status='1' where shop_name = '" + shop_name + "';"
				cur.execute(user_query)
				conn.commit()

				#Correct message so mark it 7
				user_query = "UPDATE c2b_receiver set status='7' where msg_id = '" + str(msg_id) + "';"
				cur.execute(user_query)
				conn.commit()

				location = df.index.get_indexer_for(df[df['Shop_Name']== shop_name].index)
				worksheet.update_cell(int(location[0]+2),5,4)

				continue

			#Checking if user is valid user
			user_query = "SELECT user_id, city ,status from c2b_users where shop_name = '" + shop_name + "';"
			cur.execute(user_query)
			x = cur.fetchone()
			if x is None:
			    update_query = "UPDATE c2b_receiver SET status='10' WHERE msg_id='" + str(msg_id) + "';"
			    cur.execute(update_query)
			    conn.commit()
			    continue
			if x[2] == 0:
			    update_query = "UPDATE c2b_receiver SET status='11' WHERE msg_id='" + str(msg_id) + "';"
			    cur.execute(update_query)
			    conn.commit()
			    continue

			user_id = x[0]
			user_city = x[1]

			#Checking if message format is correct
			split_msg = msg.split('-')
			if len(split_msg) != 2:
				print "there is no -"
				update_query = "UPDATE c2b_receiver SET status='3' WHERE msg_id='" + str(msg_id) + "';"
				cur.execute(update_query)
				conn.commit()
				continue
			reg_no = split_msg[0].upper()
			price = split_msg[1]
			if len(price) > 7 or not price.isdigit():
				print "You can not quote more than 10 lakhs"
				update_query = "UPDATE c2b_receiver SET status='3' WHERE msg_id='" + str(msg_id) + "';"
				cur.execute(update_query)
				conn.commit()
				continue

			price = int(price)

			#Checking if reg_no is in bikes table
			bike_query = "SELECT * from c2b_bikes where bikes_info ->> 'vehicalRegNo' = '"+ reg_no +"' and status = '2';"
			cur.execute(bike_query)
			x = cur.fetchone()
			if x is None:
				bike_query = "SELECT * from c2b_bikes where bikes_info ->> 'vehicalRegNo' = '"+ reg_no +"' and status = '3';"
				cur.execute(bike_query)
				x1 = cur.fetchone()
				if x1 is not None:
					print "Time limit exceeded"
					update_query = "UPDATE c2b_receiver SET status='4' WHERE msg_id='" + str(msg_id) + "';"
					cur.execute(update_query)
					conn.commit()
					continue

				print "There is no bike with given reg_no"
				update_query = "UPDATE c2b_receiver SET status='5' WHERE msg_id='" + str(msg_id) + "';"
				cur.execute(update_query)
				conn.commit()
				continue
			if x is not None:
				bike_id = x[0]
				lead_id = x[1]
				reginfo = json.loads(x[2])
				reg_no = reginfo.get('vehicalRegNo')
				bike_city = x[4]
				com_type = x[12]

			#Check if user belongs to same city as bike
			if user_city != bike_city:
				print "User city and bike city doesnot match"
				update_query = "UPDATE c2b_receiver SET status='6' WHERE msg_id='" + str(msg_id) + "';"
				cur.execute(update_query)
				conn.commit()
				continue

			#As all conditions are satisfied set status=1
			user_query = "UPDATE c2b_receiver set status='1' where msg_id = '" + str(msg_id) + "';"
			cur.execute(user_query)
			conn.commit()


			#Check if user has quoted on same bike, if yes then update quote
			check_query = "SELECT * from c2b_quotes where bike_id='" + str(bike_id) + "' and dealer_id='" + str(user_id) + "';"
			cur.execute(check_query)
			x = cur.fetchone()
			if x is None:
				#INSERT into quotes table
				insert_quote_query = "INSERT INTO c2b_quotes (bike_id, lead_id, dealer_id, quote, sold_to) VALUES (%s,%s,%s,%s,%s)"
				quote_data = (bike_id, lead_id, user_id, price, 0)
				cur.execute(insert_quote_query, quote_data)
				conn.commit()
			if x is not None:
				#Update the existing quote
				update_query = "UPDATE c2b_quotes SET quote='" + str(price) + "' WHERE bike_id='" + str(bike_id) + "' and dealer_id='" + str(user_id) + "';"
				cur.execute(update_query)
				conn.commit()
			query2 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + str(shop_name) + "','{\"text\" : \"Your quote is registered for bike with registration no : " + str(reg_no) + " \"}',0);"
			cur.execute(query2)
			conn.commit()


			if com_type !='c2b':
				#Post the top three quotes for this particular bike_id
				quotes_query = "SELECT dealer_id, quote from c2b_quotes where lead_id = '" + str(lead_id) + "' ORDER BY quote desc limit 3;"
				cur.execute(quotes_query)
				quotes = cur.fetchall()
				send_dict = {"Dealer1":"","Quote1":"","Dealer2":"","Quote2":"","Dealer3":"","Quote3":""}
				for index, quote in enumerate(quotes):
					print quote[0], quote[1]
					dealer_text = "Dealer" + str(index+1)
					quote_text = "Quote" + str(index+1)
					send_dict[dealer_text] = quote[0]
					send_dict[quote_text] = quote[1]
				print send_dict

				send_url = 'https://sheetsu.com/apis/v1.0/9a39e52c91d7/SrNo/' + str(lead_id)
				requests.patch(send_url, data=send_dict)
	except:
		traceback.print_exc()
