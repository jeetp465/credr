
# coding: utf-8

# In[9]:

import pandas as pd
import gspread
import psycopg2
from table_dict import *
from oauth2client.service_account import ServiceAccountCredentials
import traceback


# In[10]:

conn = psycopg2.connect(database="whatsapp", user="whatsapp_admin", password="wh@t^a**d!", host="52.76.193.246", port="5432")
cur = conn.cursor()
print "Connection Established"


# In[11]:

scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('Whatsapp C2B-3d76016fd6da.json', scope)

gc = gspread.authorize(credentials)
print "Connection to Google Sheets API Established"


# In[12]:

sh = gc.open_by_url('https://docs.google.com/spreadsheets/d/1zzfKH7Mx_7kzjil3fn4_6CdzMAhVR9wqRfbWC-HV_Kc/edit#gid=0')

worksheet_list = sh.worksheets()
print worksheet_list


# In[13]:

length = []
for worksheet in worksheet_list:
    data = worksheet.get_all_values()
    headers = data[0]
    data = data[1:]
    df = pd.DataFrame(data, columns= headers)
    length.append(len(df))


# In[14]:

bike_sold = worksheet_list[0]
incorrect_msg = worksheet_list[1]
quotes_per_bike = worksheet_list[2]
dealer_bids = worksheet_list[3]
print length


# In[15]:

query = "Select bikes_info ->> 'vehicalRegNo', bike_id from c2b_bikes where city = 'Delhi' order by bike_id asc;"
cur.execute(query)
conn.commit()
rows = cur.fetchall()
rows = rows[4:]
i = 1
total_bikes = len(rows)
for row in rows:
    reg_No = row[0]
    bike_id = row[1]
    try:
        query = "Select count(*) from c2b_quotes where bike_id =" + str(bike_id) + ";"
        cur.execute(query)
    except:
        conn.rollback()
        traceback.print_exc()

    count = cur.fetchone()
    count = count[0]
    if i > (length[2]):
        print i+1
        print reg_No, count
#         quotes_per_bike.update_cell(int(i+1),1,reg_No)
#         quotes_per_bike.update_cell(int(i+1),2,count)
    else:
        pass
    i += 1

conn.commit()


# In[ ]:

query = "Select * from c2b_receiver where status = 8 and from_name in (select shop_name from c2b_users where city = 'Delhi') order by time asc"
cur.execute(query)
conn.commit()
rows = cur.fetchall()

i = 1
for row in rows:
    message = row[1]
    from_name = row[3]

    if 'hi' in message.lower() and 'vehicle' not in message.lower():
        continue
#     print message, from_name
    split_msg = message.split('-')
    if len(split_msg) != 2:
        i += 1
        if i > (length[1]+1):
            print i
            print message + ", " + from_name
            incorrect_msg.update_cell(int(i),1,message)
            incorrect_msg.update_cell(int(i),2,from_name)
        continue
    reg_no = split_msg[0].upper()
    query = "Select * from c2b_bikes where bikes_info ->> 'vehicalRegNo' ='" + str(reg_no) + "';"
    cur.execute(query)
    conn.commit()
    x = cur.fetchone()
    if x:
        continue
    else:
        i += 1   
        if i > (length[1]+1):
            print i
            print message + ", " + from_name
            incorrect_msg.update_cell(int(i),1,message)
            incorrect_msg.update_cell(int(i),2,from_name)
    price = split_msg[1]
    price = "".join(price.split())
    price = "".join(price.split(','))
    reg_no = "".join(reg_no.split())
    try:
        price = type(int(price))
    except:
        i+=1
        if i > (length[1]+1):
            print i
            print message + ", " + from_name
            incorrect_msg.update_cell(int(i),1,message)
            incorrect_msg.update_cell(int(i),2,from_name)
        
#     i+=1


# In[18]:

query = "Select u.shop_name, q.quote, b.bikes_info ->> 'vehicalRegNo' from c2b_quotes q, c2b_users u, c2b_bikes b where u.shop_name in (Select u.shop_name from c2b_users where u.user_id = q.dealer_id and u.city = 'Delhi') and b.bike_id = q.bike_id"
cur.execute(query)
conn.commit()
rows = cur.fetchall()
i=1
data = dealer_bids.get_all_values()
headers = data[0]
data = data[1:]
df = pd.DataFrame(data,columns = headers)
df_dealer_name = df['Dealer_name']
df_reg_no = df['Reg_No']
df_reg_no = list(set(df['Reg_No']))
df_quote = df['Quote']
i =1
for row in rows:
    dealer_name = row[0]
    quote = row[1]
    reg_no = row[2]
    
    if reg_no in df_reg_no :
        continue
    else:
        i+=1
        loc = int(i) + len(df)
        print loc , str(dealer_name) + ", "+ str(quote) +", " + str(reg_no)
        dealer_bids.update_cell(int(loc),1,dealer_name)
        dealer_bids.update_cell(int(loc),2,quote)
        dealer_bids.update_cell(int(loc),3,reg_no)
#         dealer_bids.update_cell(int(i)+len(df),1,dealer_name)
#         dealer_bids.update_cell(int(i)+len(df),2,quote)
    
#     print i
#     print dealer_name, quote, reg_no
#     i+=1
#         quotes_per_bike.update_cell()


# In[20]:

query = "select bikes_info ->> 'vehicalRegNo', bikes_info ->>'model', created_time::date from c2b_bikes where city ='Delhi' and status =3 order by created_time::date asc"
cur.execute(query)
conn.commit()
rows = cur.fetchall()
data= bike_sold.get_all_values()
headers = data[0]
data = data[1:]
df = pd.DataFrame(data,columns=headers)
df_reg_no = df['Reg_No']
df_reg_no = list(set(df_reg_no))
i = 1
test = ['REST123','DLMOMOTEST1','DLMOMOTEST','TEST999']
for row in rows:
    reg_no = row[0]
    model = row[1]
    date = row[2]
    
    if reg_no in df_reg_no:
        continue
    elif reg_no in test:
        continue
    else:
        i+=1
        loc = len(df)+i
        print loc, reg_no, model, date
        bike_sold.update_cell(loc,1,reg_no)
        bike_sold.update_cell(loc,2,model)
        bike_sold.update_cell(loc,3,date)


# In[ ]:



