import json
import psycopg2
import traceback
import smtplib
import traceback
import time
import table_dict


class Scheduler:
    def __init__(self):
        self.connection = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!", host="52.74.154.2", port="5432")
        self.cursor = self.connection.cursor()

    ''' get active expired bid timings '''

    def getActiveExpiredBids(self):
        getTimings = "SELECT  bike_id, threshold, created_time, city, bikes_info FROM c2b_bikes WHERE status = 2 AND (extract(epoch FROM  current_timestamp - sms_send_time)/3600 ) * 60  > threshold "
        # getTimings = " SELECT (extract(epoch FROM  current_timestamp - created_time)/3600 ) * 60 , bike_id, threshold, created_time FROM c2b_bikes WHERE status = 0 "

        self.cursor.execute(getTimings)
        result = self.cursor.fetchall()
        print result
        return result

    def sendMaxBidMsg(self, bikeId, reg_no):
        try:
            query = "Select dealer_id, quote, 1 from c2b_quotes where bike_id = " + str(bikeId) + " order by quote DESC limit 1"
            self.cursor.execute(query)
            rows = self.cursor.fetchone()
            if len(rows) > 0:
                query = "Select shop_name from c2b_users where user_id ='" + str(rows[0]) + "';"
                self.cursor.execute(query)
                shop_name = self.cursor.fetchone()

                query2 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + str(shop_name[0]) + "','{\"text\" : \"You are the highest bidder for bike with registration no : " + str(reg_no) + " \"}',0);"
                self.cursor.execute(query2)

                query = "Update c2b_quotes set sold_to = 1 where bike_id = " + str(bikeId) + " and dealer_id =" + str(rows[0])
                self.cursor.execute(query)
                self.connection.commit()
        except:
            traceback.print_exc()

    '''send bike bidding close message to dealer'''

    def sendBidClosedMessage(self, city, reg_no):
        print city
        try:
            max_quote = "select quote from c2b_quotes where bike_id in (select bike_id from c2b_bikes where bikes_info ->> 'vehicalRegNo' = '"+str(reg_no)+"' order by bike_id desc limit 1) order by quote desc limit 1"
            self.cursor.execute(max_quote)
            quote = cur.fetchone()
            quote = quote[0]
            insertBidClosed = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + str(city) + "','{\"text\" : \"Bidding for bike with registration no : " + str(reg_no) + " is closed. The highest bid is Rs. "+ str(quote) +"\"}',0);"
            self.cursor.execute(insertBidClosed)
            self.connection.commit()
        except:
            self.connection.rollback()
            traceback.print_exc()

    ''' update expired bids to status 3'''

    def updateExpiredBid(self, activeBids):
        for bid in activeBids:
            bikeId = bid[0]
            city = bid[3]
            bikeData = json.loads(bid[4])
            try:
                reg_no = bikeData.get('vehicalRegNo')
                updateBids = "UPDATE c2b_bikes SET status = 3 WHERE bike_id = %s "
                self.cursor.execute(updateBids, [bikeId])
                self.connection.commit()
                self.sendBidClosedMessage(city, reg_no)
                self.sendMaxBidMsg(bikeId, reg_no)

            except:
                self.connection.rollback()
                traceback.print_exc()


if __name__ == '__main__':

    while (1):
        try:
            SchedulerObject = Scheduler()
            activeBids = SchedulerObject.getActiveExpiredBids()
            SchedulerObject.updateExpiredBid(activeBids)
        except:
            traceback.print_exc()
        import time

        time.sleep(60)
