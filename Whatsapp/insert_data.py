import psycopg2
import json
import re
import traceback
import time
import ast
import smtplib
import traceback
from table_dict import c2b_bikes, c2b_receiver

# conn = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!", host="52.74.154.2", port="5432")
conn = psycopg2.connect(database="whatsapp", user="whatsapp_admin", password="wh@t^a**d!", host="52.76.193.246", port="5432")
cur = conn.cursor()
print "Connection Established"

while (1):

    try:
        ######################################################################################################################
        #######################################     SELECTING BIKE DATA FROM C2B_BIKES     ###################################
        ######################################################################################################################

        query = "Select * from c2b_bikes where status = 0"
        cur.execute(query)

        rows = cur.fetchall()
        # print rows
        data = dict()
        # print rows
        i = 0

        for row in rows:

            image_data_list = ast.literal_eval(str(row[int(c2b_bikes.get("images"))]))

            # print row[2]
            print row[3]
            bike_data = json.loads(row[int(c2b_bikes.get("bikes_info"))])
            valuation = json.loads(row[int(c2b_bikes.get("valuation_info"))])

            bike_info = "{\"text\" : \"Vehicle- " + bike_data.get('make') + " " + bike_data.get(
                'model') + " " + bike_data.get('variant') + ", Registration No- " + bike_data.get(
                'vehicalRegNo') + "\"}"
            # print bike_info
            # print valuation
            valuation_info = "{\"text\" : \"Back Suspension- " + valuation.get('backSuspension') + ", Chassis Alignment- " + valuation.get('chassisAlignment') + ", Finance- " + valuation.get('finance') + ", Front Suspension-" + valuation.get('frontSuspension') + ", No. of owners- " + str(valuation.get('NoOfOwners')) + ", OriginalRC-" + valuation.get('orignalRc') + ", Color of Smoke-" + valuation.get('colorOfSmoke') + ", noisy engine- " + valuation.get('noisyEngine') + ", NOC- " + valuation.get('noc') + ", Insurance- " + valuation.get('insurance') + "\"}"

            try:
                query1 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + str(row[int(c2b_bikes.get("city"))]) + "','" + bike_info + "',0);"
                cur.execute(query1)
                conn.commit()

                query1 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + str(row[int(c2b_bikes.get("city"))]) + "','" + valuation_info + "',0);"
                cur.execute(query1)
                conn.commit()
                count_img_list = len(image_data_list) -1
                for i in range(len(image_data_list)):
                    image_info = {}
                    for key, value in image_data_list[i].items():
                        image_info["image"] = value

                    image_info["caption"] = bike_data.get('vehicalRegNo') + "-" + str(i)
                    print image_info
                    if i == count_img_list:
                        image_info["bike_id"] = str(row[int(c2b_bikes.get("bike_id"))])

                    query1 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES (%s,%s,%s)"
                    cur.execute(query1, [str(row[int(c2b_bikes.get("city"))]), str(image_info), 0])
                query1 = "Update c2b_bikes set status = 2,sms_send_time =NULL where bike_id = '" + str(
                    row[int(c2b_bikes.get("bike_id"))]) + "';"
                cur.execute(query1)
                conn.commit()
            except:
                conn.rollback()
                traceback.print_exc()
                query1 = "Update c2b_bikes set status = 1 ,sms_send_time =NULL where bike_id = '" + str(
                    row[int(c2b_bikes.get("bike_id"))]) + "';"
                cur.execute(query1)
                conn.commit()
            # tested

        query = "Select * from c2b_bikes where sold_Status != 0"
        cur.execute(query)
        rows = cur.fetchall()

        for row in rows:
            bike_id = row[int(c2b_bikes.get("bike_id"))]                # getting bike_id of the bike marked after bidding closed
            reg_no = json.loads(row[int(c2b_bikes.get("bikes_info"))]).get('vehicalRegNo')
            
            shop_name_query = "select shop_name from c2b_users where user_id in (select dealer_id from c2b_quotes where bike_id ="+str(bike_id)+" and sold_to = '2')"
            cur.execute(shop_name_query)
            conn.commit()
            
            shop_name = cur.fetchone()
            
            try:
                if row[int(c2b_bikes.get("sold_status"))] == 1:          # bike has been sold
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" +str(shop_name[0])+"','{\"text\":\"The vehicle with Registration No: "+str(reg_no)+" has been sold to you.\"}',0)"
                    cur.execute(query3)
                    update_query = "Update c2b_bikes set status = 4 where bike_id =" +str(bike_id)
                    cur.execute(update_query)
                    conn.commit()
                elif row[int(c2b_bikes.get("sold_status"))] == 2:       # seller did not agree to sell the bike
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" +str(shop_name[0])+"','{\"text\":\"The seller of vehicle with Registration No: "+str(reg_no)+" did not agree to sell the vehicle.\"}',0)"
                    cur.execute(query3)
                    update_query = "Update c2b_bikes set status = 4 where bike_id =" +str(bike_id)
                    cur.execute(update_query)
                    conn.commit()
                elif row[int(c2b_bikes.get("sold_status"))] == 3:       # seller is still deciding
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" +str(shop_name[0])+"','{\"text\":\"The seller of vehicle with Registration No: "+str(reg_no)+" is still deciding.\"}',0)"
                    cur.execute(query3)
                    update_query = "Update c2b_bikes set status = 4 where bike_id =" +str(bike_id)
                    cur.execute(update_query)
                    conn.commit()   
                elif row[int(c2b_bikes.get("sold_status"))] == 4:       # the bike is sold through credr website
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" +str(shop_name[0])+"','{\"text\":\"The seller of vehicle with Registration No: "+str(reg_no)+" did not agree to sell the vehicle.\"}',0)"
                    cur.execute(query3)
                    update_query = "Update c2b_bikes set status = 4 where bike_id =" +str(bike_id)
                    cur.execute(update_query)
                    conn.commit()
            except:
                conn.rollback()
                traceback.print_exc()

        #####################################################################################################################
        ##########################################     SELECTING DATA FROM RECEIVER     #####################################
        #####################################################################################################################

        query = "Select * from c2b_receiver where status not in (1,8)"

        cur.execute(query)

        rows = cur.fetchall()
        print rows
        for row in rows:
            try:
                if row[int(c2b_receiver.get("status"))] == 3:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" :\"You have been successfully registered. The message should be in the format registration_no-quote for eg. MA96AC1683-20089.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[int(c2b_receiver.get("status"))] == 4:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\":\"The bidding process for the above registration number is closed.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[int(c2b_receiver.get("status"))] == 5:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" : \"bike does not exist. Please recheck the registration number\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[4] == 6:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" : \"Above registration_no of bike does not belong to your city.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[int(c2b_receiver.get("status"))] == 7:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" : \"You have been successfully registered.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[int(c2b_receiver.get("status"))] == 10:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" : \"Sorry you are not in our user list.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
                elif row[int(c2b_receiver.get("status"))] == 11:
                    query3 = "Insert into c2b_sender (shop_name,message,sms_status) VALUES ('" + row[
                        int(c2b_receiver.get("from_name"))] + "','{\"text\" : \"Please register yourself by sending 'Hi'.\"}',0);"
                    cur.execute(query3)
                    conn.commit()
                    query = "update c2b_receiver set status = 8 where msg_id = '" + str(row[int(c2b_receiver.get("msg_id"))]) + "';"
                    cur.execute(query)
                    conn.commit()
            except:
                conn.rollback()
                traceback.print_exc()
    except:
        traceback.print_exc()