% Author: Vicente Ordonez
%         Stony Brook University - State University of New York
% 
% This code implements my version of the salient object detector first 
% presented by Microsoft Research Asia in:
% "Tie Liu, Jian Sun, Nan-Ning Zheng, Xiaoou Tang and Heung-Yeung Shum.
% Learning to Detect A Salient Object. In Proc. IEEE CVPR 2007"
% 
% Execute this README.m file to get a sample result.
%
addpath(genpath('.'));
vl_setup

image = imread('leaf.jpg');
image = rescale_max_size(image, 200);
%imshow(image);
% Visual saliency using the MSRA Salient Object Detection.
fprintf('Calculating saliency map...\n'); tic

% Compute features.
[contrast_map center_surround_map color_spatial_map] = ...
    extract_salient_feature_maps(image);

% Infer saliency mask.
[mask mask_2 saliency_score] = infer_salient_map(contrast_map, ...
                                                 center_surround_map, ...
                                                 color_spatial_map);

fprintf('Saliency map score %f\n', saliency_score);
imshow([im2double(image) repmat(contrast_map, [1 1 3]);
        repmat(center_surround_map, [1 1 3]) repmat(color_spatial_map, [1 1 3])]);
figure;imshow([im2double(image) repmat(mask, [1 1 3])]);
fprintf('Computation took %f\n', toc);

