import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('MH04GY9096_1.jpg')

#print type(img)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(img,50,100)

D = np.copy(edges)
fg = np.copy(edges)
fg_col = np.copy(img)

# for i in xrange(fg_col.shape[0]):
# 	for j in xrange(fg_col.shape[1]):
# 		if fg_col[i][j] == gray[i][j]:
# 			continue
# 		else:
# 			fg_col[i][j] = 0
# #print type(D)

# for i in xrange(D.shape[0]):
# 	for j in xrange(D.shape[1]):
# 		if D[i][j] == 1:
# 			D[i][j] == i
# 		else:
# 			continue
# max_index = []
# max_val = 0
# min_index = []
# # print D.shape[0],D.shape[1]
# for j in xrange(D.shape[1]):
# 	max_val = 0
# 	count = 0
# 	for i in xrange(D.shape[0]):
# 		if D[i][j] > max_val :
# 			max_val = D[i][j]
# 			if count == 0:
# 				min_index.append(D[i][j])
# 				count = 1
# 		else :
# 			# min_index.append(max_val)
# 			continue
# 	if count == 0 :
# 		min_index.append(D[i][j])
# 		count = 1
# 	max_index.append(max_val)

# # print len(max_index)
# # print len(min_index)
# for j in range(len(min_index)):
# 	k1 = min_index[i]
# 	k2 = max_index[i]
# 	for i in range(k1,k2+1):
# 		fg[i][j] = 1

# for j in xrange(D.shape[1]):
# 	for i in xrange(D.shape[0]):
# 		if fg[i][j] == 1:
# 			continue
# 		else:
# 			fg_col[i][j] = 0



plt.subplot(221),plt.imshow(gray,'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(fg_col)
plt.title('Foreground'), plt.xticks([]), plt.yticks([])

plt.show()
