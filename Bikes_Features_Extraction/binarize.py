
# coding: utf-8

# In[1]:

import numpy as np
import glob, os
from tqdm import tqdm
from PIL import Image
from skimage.filters import threshold_adaptive
import scipy.misc


# In[2]:

#imgs_list = glob.glob('/home/vikas/Documents/projects/credR/rc/images/*')


# In[4]:

#for file_name in tqdm(imgs_list):
file_path = "/home/jeet/Documents/CredR/Bikes_Features_Extraction/test1.jpg"
img = Image.open(file_path)

bw_img = np.array(img.convert("L"))

block_size = 40
binary_adaptive = threshold_adaptive(bw_img, block_size, offset=10)

save_bin_path = "/home/jeet/Desktop/" + file_name
scipy.misc.imsave(save_bin_path, binary_adaptive)
