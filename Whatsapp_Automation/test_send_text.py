# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import os

class TestSendText(unittest.TestCase):
    def setUp(self):
        # self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        # self.driver = webdriver.Firefox(self.profile)
        self.chromedriver = "/home/jeet/.config/google-chrome/Default"
        os.environ["webdriver.chrome.driver"] = self.chromedriver
        self.driver = webdriver.Chrome(self.chromedriver)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_send_text(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        time.sleep(5)
        start_time = time.time()
        driver.find_element_by_xpath("//span[@title='Vikas Reddy']").click()
        print time.time()-start_time
        driver.find_element_by_xpath("//*[@id='main']/footer/div/div[1]/div/div[2]").send_keys('Hi I am Jeet')
        print time.time()-start_time
        driver.find_element_by_xpath("//div[@id='main']/footer/div/button[2]").click()
        print time.time()-start_time 
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
