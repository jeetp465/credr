import re 
import HTMLParser 
import xml.etree.ElementTree as ET
import os
from bs4 import BeautifulSoup

html_doc = """<div class="animate-enter1 drawer-section-expand well" style="min-height:240px;" data-list-scroll-container="true" data-reactid=".0.0:$main.3.2.$=10.1:1.1"> 
<div class="drawer-section-title title-underlined" data-reactid=".0.0:$main.3.2.$=10.1:1.1.0"> 
<div class="col-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.0.0">Recipients</div> 
<div class="col-side" data-reactid=".0.0:$main.3.2.$=10.1:1.1.0.1">7</div> 
</div> 
<div class="drawer-section-body drawer-section-body-expand chatlist" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1"> 
<div class="infinite-list " data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0"> 
<div class="infinite-list-viewport" style="height:504px;" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0"> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:1;height:72px;will-change:transform;transform:translate3d(0, 500%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=917506242475%40c.us&amp;i=1427648619" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Vijay" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.0.0.0">Vijay</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1"> 
<div title="Hey there! I am *not* using WhatsApp." class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1.0.0">Hey there! I am *not* using WhatsApp.</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506242475@c=1us.$917506242475@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:5;height:72px;will-change:transform;transform:translate3d(0, 100%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=917506683090%40c.us&amp;i=1463835129" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Mohit" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.0.0.0">Mohit</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1"> 
<div title="Available" class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1.0.0">Available</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$917506683090@c=1us.$917506683090@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:4;height:72px;will-change:transform;transform:translate3d(0, 200%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Saboo" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.0.0.0">Saboo</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1"> 
<div title="" class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1.0"> 
<noscript data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1.0.0"> 
</noscript> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$918450940001@c=1us.$918450940001@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:2;height:72px;will-change:transform;transform:translate3d(0, 400%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=919002325920%40c.us&amp;i=1463765605" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Sourav Iitb" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.0.0.0">Sourav Iitb</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1"> 
<div title="..." class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1.0.0">...</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919002325920@c=1us.$919002325920@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:3;height:72px;will-change:transform;transform:translate3d(0, 300%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=919021463567%40c.us&amp;i=1458293747" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Sailesh" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.0.0.0">Sailesh</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1"> 
<div title="Friendship doubles your joy and divides your sorrow" class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1.0.0">Friendship doubles your joy and divides your sorrow</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919021463567@c=1us.$919021463567@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="infinite-list-item infinite-list-item-transition" style="z-index:0;height:72px;will-change:transform;transform:translate3d(0, 600%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=919742976025%40c.us&amp;i=1462120323" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Yogesh Laddha" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.0.0.0">Yogesh Laddha</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1"> 
<div title="" class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1.0.0"> 
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="emoji emojiordered1373" draggable="false"> 
</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919742976025@c=1us.$919742976025@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
<div class="first infinite-list-item infinite-list-item-transition" style="z-index:6;height:72px;will-change:transform;transform:translate3d(0, 0%, 0);" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us"> 
<div tabindex="-1" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us"> 
<div class="contact chat" data-ignore-capture="any" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0"> 
<div class="chat-avatar" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.0"> 
<div class="avatar icon-user-default" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.0.0"> 
<img src="https://dyn.web.whatsapp.com/pp?t=s&amp;u=919920909752%40c.us&amp;i=1462092748" class="avatar-image is-loaded" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.0.0.0"> 
</div> 
</div> 
<div class="chat-body" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1"> 
<div class="chat-main" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.0"> 
<div class="chat-title" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.0.0"> 
<span class="emojitext ellipsify" dir="auto" title="Cliffton" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.0.0.0">Cliffton</span> 
</div> 
</div> 
<div class="chat-secondary" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1"> 
<div title="" class="chat-status ellipsify" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1.0"> 
<span class="emojitext ellipsify" dir="auto" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1.0.0"> 
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="emoji emojiordered1413" draggable="false" alt=""> 
</span> 
</div> 
<div class="chat-meta" data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1.1"> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1.1.1"> 
</span> 
<span data-reactid=".0.0:$main.3.2.$=10.1:1.1.1.0.0.$919920909752@c=1us.$919920909752@c=1us.0.1.1.1.2"> 
</span> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div>
"""

import lxml.html
info=lxml.html.fromstring(html_doc)

for status in info.xpath("//div//div[@class='chat-secondary']"):
	status.getparent().remove(status)


result=info.xpath("//div//span[@class='emojitext ellipsify']")

print len(result)
data = []
for element in result:
	element = lxml.html.tostring(element)
	element = element.split('>')
	element = element[1].split('<')
	data.append(element[0])

data = data[1:]
print data	
# lxml.html.tostring(elm
# soup = BeautifulSoup(html_doc,'html.parser')
# data = soup.get_text()
# data_final = data.split('\n')
# data_final = [x for x in data_final if x]

# for data in data_final[2:]:
# 	print data