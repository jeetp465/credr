# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import psycopg2
import json
import pyautogui
import os
import urllib
import webbrowser

########################################################################################################################
#########################################    CONNEDCTING TO DATABASE   #############################################
########################################################################################################################

conn = psycopg2.connect(database="postgres", user="postgres", password="123", host="127.0.0.1", port="5432")
cur = conn.cursor()

########################################################################################################################
#############################################    UPDATING SENDERS TABLE    #############################################
########################################################################################################################

def update(name):

    query = "update sender SET sms_status = 1 WHERE shop_name = '" + str(name) + "';"
    cur.execute(query)  
    conn.commit()

########################################################################################################################
#################################################   DOWNLOADING IMAGE   ################################################
########################################################################################################################

def download_image(image_sources):

    image_source = str(image_sources)
    urllib.urlretrieve(image_source,"/home/jeet/CredR/Whatsapp_Automation/images/upload" + str(i) +".jpg")
    print "image Downloaded"

########################################################################################################################
#################################################    BROADCASTING TEXT   ###############################################
########################################################################################################################
        
# class SendText(unittest.TestCase):
    
#     sending_to = "random"
#     text = "random"

#     # def test_input(self):
#     #     print 'broadcasting_list ',self.sending_to
#     #     print 'text ',self.text

#     def setUp(self):
#         self.profile = webdriver.FirefoxProfile('/home/jeet/.config/google-chrome/6g4wnvg7.default')
#         self.driver = webdriver.Firefox(self.profile)
#         # self.profile = webdriver.Chrome('/home/jeet/.config/google-chrome/Default')
#         # self.driver = webdriver.Chrome(self.profile)
#         # self.chromedriver = "/home/jeet/.config/google-chrome/Default"
#         # os.environ["webdriver.chrome.driver"] = self.chromedriver
#         # self.driver = webdriver.Chrome(chromedriver)
#         self.driver.implicitly_wait(30)
#         self.base_url = "https://web.whatsapp.com/"
#         self.verificationErrors = []
#         self.accept_next_alert = True
    
#     def test_send_text(self):
#         driver = self.driver
#         driver.get(self.base_url + "/")
#         time.sleep(2)
#         start_time = time.time()
#         driver.find_element_by_xpath("//span[@title='" + self.sending_to + "']").click()
#         driver.find_element_by_xpath("//*[@id='main']/footer/div/div[1]/div/div[2]").send_keys(self.text)
#         driver.find_element_by_xpath("//div[@id='main']/footer/div/button[2]").click()
#         driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
#         print time.time() - start_time 

#     def is_element_present(self, how, what):
#         try: self.driver.find_element(by=how, value=what)
#         except NoSuchElementException as e: return False
#         return True
    
#     def is_alert_present(self):
#         try: self.driver.switch_to_alert()
#         except NoAlertPresentException as e: return False
#         return True
    
#     def close_alert_and_get_its_text(self):
#         try:
#             alert = self.driver.switch_to_alert()
#             alert_text = alert.text
#             if self.accept_next_alert:
#                 alert.accept()
#             else:
#                 alert.dismiss()
#             return alert_text
#         finally: self.accept_next_alert = True

#     def tearDown(self):
#         time.sleep(3)
#         self.driver.quit()
#         self.assertEqual([], self.verificationErrors)
    

# ########################################################################################################################
# ################################################    BROADCASTING IMAGES   ##############################################
# ########################################################################################################################

# class TestSendImage(unittest.TestCase):

#     sending_to = "random"
#     caption = "caption"
    
#     # def test_image(self):
#     #     print "sending to :", self.sending_to
#     #     print "caption :", self.caption
    
#     def setUp(self):
#         self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
#         self.driver = webdriver.Firefox(self.profile)
#         self.driver.implicitly_wait(30)
#         self.base_url = "https://web.whatsapp.com/"
#         self.verificationErrors = []
#         self.accept_next_alert = True
    
#     def test_(self):
#         driver = self.driver
#         driver.get(self.base_url + "/")
#         driver.find_element_by_xpath("//span[@title='"+ self.sending_to +"']").click()
#         driver.find_element_by_css_selector("button.icon.icon-clip").click()
#         driver.find_element_by_css_selector("button.menu-icons-item").click()
#         driver.find_element_by_xpath("(//input[@type='file'])[2]")
#         pyautogui.press('enter')
#         time.sleep(2)
#         pyautogui.typewrite(self.caption)
#         time.sleep(1)
#         driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
#         #time.sleep(5)
#         driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()

#     def is_element_present(self, how, what):
#         try: self.driver.find_element(by=how, value=what)
#         except NoSuchElementException as e: return False
#         return True
    
#     def is_alert_present(self):
#         try: self.driver.switch_to_alert()
#         except NoAlertPresentException as e: return False
#         return True
    
#     def close_alert_and_get_its_text(self):
#         try:
#             alert = self.driver.switch_to_alert()
#             alert_text = alert.text
#             if self.accept_next_alert:
#                 alert.accept()
#             else:
#                 alert.dismiss()
#             return alert_text
#         finally: self.accept_next_alert = True

#     def tearDown(self):
#         time.sleep(3)
#         self.driver.quit()
#         self.assertEqual([], self.verificationErrors)
    
#     # fname = os.listdir('/home/jeet/CredR/Whatsapp_Automation/images')
#     # os.remove('/home/jeet/CredR/Whatsapp_Automation/images/' + str(fname[0]))
#     # fname.pop(0)



# ########################################################################################################################
# #################################################   DEFINING TESTSUITES   ##############################################
# ########################################################################################################################

# def suite1():

#     test_suite = unittest.TestSuite()
#     test_suite.addTest(unittest.makeSuite(SendText))
#     return test_suite

# def suite2():

#     test_suite = unittest.TestSuite()
#     test_suite.addTest(unittest.makeSuite(TestSendImage))
#     return test_suite

########################################################################################################################
##################################################    RUNNING UNITTEST   ###############################################
########################################################################################################################

class New(unittest.TestCase):

    def setUp(self):
        # self.profile = webdriver.Chrome('/home/jeet/.config/google-chrome/Default')
        # self.driver = webdriver.Chrome(self.profile)
        # self.chromedriver = "/home/jeet/.config/google-chrome/Default"
        # os.environ["webdriver.chrome.driver"] = self.chromedriver
        # self.driver = webdriver.Chrome(chromedriver)
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_new(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        time.sleep(2)
        
        while(1):
            #global data,senders_list
            driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
            try:
                elms = driver.find_elements_by_xpath("//*[@class='unread chat']")
            except:
                elms = [] 
                pass  
            
            if len(elms) > 0:
                #vikas's code
                #Go through all unread chats
                for elm in elms:                                                   
                    text = elm.text
                    words = text.splitlines()
                    
                    elm.click()                                                     

                    #Go through all unread message in each chat
                    msgs = driver.find_elements_by_xpath("//div[@class='message-list']/div") 
                    for msg in msgs[-1*int(words[3]):]:                             
                        msg.click() 
                        message_list = msg.text.splitlines()
                        message_list[1] = message_list[1].replace('\u2060', '')
                        print "from: " + str(words[0]).replace(" ", "").replace("+", "") + " == " + message_list[0] + " at " + message_list[1]
                        
                        shop_name = str(words[0])

                        #Dumps message in receiver table
                        msg = str(message_list[0])
                        data = (msg, shop_name, '0')
                        query="INSERT INTO receiver (msg, from_name, status) VALUES (%s,%s,%s)"
                        cur.execute(query,data)
                        conn.commit()
                        
                    driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
            else :
                query = "Select * from sender where sms_status = 0"
                cur.execute(query)

                rows = cur.fetchall()

                senders_list = []                   #list of senders
                data = []                           #list to store data   

                i=0

                for row in rows:
                    temp = row[0]
                    senders_list.append(temp)
                    #print type(row[1])
                    data.append(json.loads(row[1]))
                    #print type(data[i])
                    i = i +1

                j = 0 
                print len(data), len(senders_list)


                if len(data) > 0 and len(senders_list) > 0:
                    for j in range(0,len(data)):
                        self.sending_to = senders_list[j]
                        print type(data[j])
                        if data[j].get('text') != None :
                            self.text = data[j].get('text')
                        else:
                            self.caption = data[j].get('caption') 
                            self.image_source = data[j].get('image')

                        if data[j].get('text') != None:
                            #send text
                            driver.find_element_by_xpath("//span[@title='" + self.sending_to + "']").click()
                            driver.find_element_by_xpath("//*[@id='main']/footer/div/div[1]/div/div[2]").send_keys(self.text)
                            driver.find_element_by_xpath("//div[@id='main']/footer/div/button[2]").click()
                            driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
                        else:
                            #send image
                            download_image(self.image_source)
                            driver.find_element_by_xpath("//span[@title='"+ self.sending_to +"']").click()
                            driver.find_element_by_css_selector("button.icon.icon-clip").click()
                            driver.find_element_by_css_selector("button.menu-icons-item").click()
                            driver.find_element_by_xpath("(//input[@type='file'])[2]")
                            pyautogui.press('enter')
                            time.sleep(2)
                            pyautogui.typewrite(self.caption)
                            time.sleep(1)
                            driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
                            #time.sleep(5)
                            driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
                        update(senders_list[j])
                        j = j + 1
                    senders_list = []
                    data = []
                else:
                    continue

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        time.sleep(3)
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)



if __name__ == "__main__":

    unittest.main()

    # i = 0 
    # for i in range(len(data)):
    #     if data[i].get('text') != None:
    #         SendText.sending_to = senders_list[i]
    #         SendText.text = data[i].get('text')
    #         runner=unittest.TextTestRunner()
    #         runner.run(suite1())
    #     else :
    #         TestSendImage.sending_to = senders_list[i]
    #         TestSendImage.caption = data[i].get('caption')
    #         image_sources.append(data[i].get('image'))
    #         download_image(image_source)
    #         runner=unittest.TextTestRunner()
    #         runner.run(suite2())
    #     update(senders_list[i])
    #     i = i + 1
        