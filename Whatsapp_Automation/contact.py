# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Contact(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://mail.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_contact(self):
        driver = self.driver
        driver.get(self.base_url + "/mail/u/2/#contacts")
        driver.find_element_by_id(":32").click()
        driver.find_element_by_css_selector("input.R5.Rh").clear()
        driver.find_element_by_css_selector("input.R5.Rh").send_keys("Jeet")
        driver.find_element_by_css_selector("span.aco.acm > input.R5").click()
        driver.find_element_by_css_selector("span.acm > input.R5").clear()
        driver.find_element_by_css_selector("span.acm > input.R5").send_keys("8400394962")
        driver.find_element_by_css_selector("span.aco.acm > input.R5").click()
        driver.find_element_by_css_selector("span.acm > input.R5").clear()
        driver.find_element_by_css_selector("span.acm > input.R5").send_keys("8175992040")
        driver.find_element_by_id(":32").click()
        driver.find_element_by_css_selector("span.acm > input.R5.Rh").clear()
        driver.find_element_by_css_selector("span.acm > input.R5.Rh").send_keys("Jeet-1")
        driver.find_element_by_css_selector("span.aco.acm > input.R5").click()
        driver.find_element_by_css_selector("span.acm > input.R5").clear()
        driver.find_element_by_css_selector("span.acm > input.R5").send_keys("9878724234")
        driver.find_element_by_css_selector("span.aco.acm > input.R5").click()
        driver.find_element_by_css_selector("span.acm > input.R5").clear()
        driver.find_element_by_css_selector("span.acm > input.R5").send_keys("8723642345")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
