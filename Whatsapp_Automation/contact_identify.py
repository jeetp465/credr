# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import time
from lxml import html
import lxml
import requests
import psycopg2

conn = psycopg2.connect(database="whatsapp_bike", user="whatsapp_admin", password="wh@t^a**d!",host="52.74.154.2", port="5432")
cur = conn.cursor()

class ContactIdentify(unittest.TestCase):
    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_contact_identify(self):
        global data
        driver = self.driver
        driver.get(self.base_url + "/")
        contact_list = []
        driver.find_element_by_xpath("//span[@title='Pune']").click()
        time.sleep(1)
        driver.find_element_by_css_selector("h2.chat-title > span.emojitext.ellipsify").click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//div[@class='drawer-section-body drawer-section-body-expand chatlist']")
        html_info = element.get_attribute('innerHTML')
        info=lxml.html.fromstring(html_info)

        text = info.xpath("//div//div[@class='chat-secondary']")
        for status in text:
            status.getparent().remove(status)

        result=info.xpath("//div//span[@class='emojitext ellipsify']")
        data = []
        for element in result:
            element = lxml.html.tostring(element)
            element = element.split('>')
            element = element[1].split('<')
            data.append(element[0])
        try:
            query = "Select shop_name from c2b_users where status = 1"
            cur.execute(query)
            all_data = cur.fetchall()
            refresh_data = [x for x in all_data if x not in data]
            print refresh_data 

            query = "Insert into c2b_sender (shop_name,message,sms_status) values ('Jeet CredR','{\"text\" : \""+ refresh_data +"\"}',0)"
            cur.execute(query)
            conn.commit()
        except:
            conn.rollback()
            traceback.print_exc()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
