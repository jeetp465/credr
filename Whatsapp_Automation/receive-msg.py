# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, lxml
import psycopg2
import json

class ReceiveMsg(unittest.TestCase):
    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_receive_msg(self):
        driver = self.driver
        driver.get(self.base_url)

        conn = psycopg2.connect(database="postgres", user="postgres", password="123", host="127.0.0.1", port="5432")
        cur = conn.cursor()
        
        while(1):
            try:
                driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
                elms = driver.find_elements_by_xpath("//*[@class='unread chat']")  

                #Go through all unread chats
                for elm in elms:                                                   
                    text = elm.text
                    words = text.splitlines()
                    
                    elm.click()                                                     

                    #Go through all unread message in each chat
                    msgs = driver.find_elements_by_xpath("//div[@class='message-list']/div") 
                    for msg in msgs[-1*int(words[3]):]:                             
                        msg.click() 
                        message_list = msg.text.splitlines()
                        message_list[1] = message_list[1].replace('\u2060', '')
                        print "from: " + str(words[0]).replace(" ", "").replace("+", "") + " == " + message_list[0] + " at " + message_list[1]
                        
                        shop_name = str(words[0])

                        #Dumps message in receiver table
                        msg = str(message_list[0])
                        data = (msg, shop_name, '0')
                        query="INSERT INTO receiver (msg, from_name, status) VALUES (%s,%s,%s)"
                        cur.execute(query,data)
                        conn.commit()
                        
                    driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
                    
                rev_check = "SELECT * from receiver where status!='6';"
                cur.execute(check_query)
                rows = cur.fetcall()
                if len(rows) != 0:
                    #your code
                    for row in rows :
                        try:
                            if row[4] == 3:
                                query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','The message is in wrong format',0);"
                                cur.execute(query3)
                                conn.commit()
                            elif row[4] == 4:
                                query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','The bidding process has been terminated',0);"
                                cur.execute(query3)
                                conn.commit()
                            elif row[4] == 5:
                                query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','The bike does not exist',0);"
                                cur.execute(query3)
                                conn.commit()
                        except:
                            conn.rollback()
                            traceback.print_exc()
                        query = "update receiver set status = 6 where from_name = '" + str(row[3]) + "';"
                        cur.execute(query)
                        conn.commit()

                quotes_check = "SELECT * from quotes where sold_to='1';"
                cur.execute(quotes_check)
                rows = cur.fetcall()
                if len(rows) != 0:
                    #your code
                    for i in range(len(rows)):
                        for row in rows :
                            try :
                                if row[0][0] == rows1[i][0] :
                                    query2 = "Insert into sender (shop_name,message) VALUES ('" + str(rows1[i][0]) + "','The bike with bike id : " + str(rows1[i][1]) + " has been sold to you.');"
                                    cur.execute(query2)
                                    conn.commit()
                                else :
                                    query2 = "Insert into sender (shop_name,message) VALUES ('" + str(rows1[i][0]) + "','The bike with bike id : " + str(rows1[i][1]) + " has been sold.');"
                                    cur.execute(query2)
                                    conn.commit()
                            except:
                                conn.rollback()
                                traceback.print_exc()
                                    
            except:
                pass

        
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main() 