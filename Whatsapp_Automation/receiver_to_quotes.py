import psycopg2, datetime


conn = psycopg2.connect(database="postgres", user="postgres", password="123", host="127.0.0.1", port="5432")
cur = conn.cursor()
while (1):
	msgs_query = "SELECT * from receiver where status = '0';"
	cur.execute(msgs_query)
	replies = cur.fetchall()
	for reply in replies:
		msg_id = reply[0]
		msg = reply[1]
		time = reply[2]
		shop_name = reply[3]

		#Verify user
		if msg.lower() == 'hi':
			user_query = "UPDATE users set status='1' where shop_name = '" + shop_name + "';"
			cur.execute(user_query)
			conn.commit()

			#Correct message and mark it 1
			user_query = "UPDATE receiver set status='1' where msg_id = '" + str(msg_id) + "';"
			cur.execute(user_query)
			conn.commit()

			continue

		#Checking if user is valid user
		user_query = "SELECT user_id from users where shop_name = '" + shop_name + "' and status = '1';"                 
		cur.execute(user_query)
		x = cur.fetchone()
		if x is None:
		    update_query = "UPDATE receiver SET status='2' WHERE msg_id='" + str(msg_id) + "';"
		    cur.execute(update_query)
		    conn.commit()
		    continue
		if x is not None: # yes user
			user_id = x[0]

		#Checking if message format is correct
		split_msg = msg.split('-')
		if len(split_msg) != 2:
			print "there is no -"
			update_query = "UPDATE receiver SET status='3' WHERE msg_id='" + str(msg_id) + "';"
			cur.execute(update_query)
			conn.commit()
			continue
		reg_no = split_msg[0]
		price = split_msg[1]

		if price.isdigit() == False:
			print "the part after - should be a number"
			update_query = "UPDATE receiver SET status='3' WHERE msg_id='" + str(msg_id) + "';"
			cur.execute(update_query)
			conn.commit()
			continue
		price = int(price)

		#Checking if reg_no is in bikes table
		bike_query = "SELECT * from bikes where registration_no = '" + reg_no + "' and status = '2';"
		cur.execute(bike_query)
		x = cur.fetchone()
		if x is None:
			print "There is no bike with given reg_no"
			update_query = "UPDATE receiver SET status='5' WHERE msg_id='" + str(msg_id) + "';"
			cur.execute(update_query)
			conn.commit()
			continue
		if x is not None:
			bike_id = x[0]
			reg_no = x[1]
			created_time = x[6]
			threshold = x[8]

		#Check if time < created_time + threshold
		if time > created_time + datetime.timedelta(minutes = threshold):
			print "Time limit exceeded"
			update_query = "UPDATE receiver SET status='4' WHERE msg_id='" + str(msg_id) + "';"
			cur.execute(update_query)
			conn.commit()	
			continue

		
		#As all conditions are satisfied set statu=1
		user_query = "UPDATE receiver set status='1' where msg_id = '" + str(msg_id) + "';"
		cur.execute(user_query)
		conn.commit()
		
		#Check if user has quoted on same bike, if yes then update quote
		check_query = "SELECT * from quotes where bike_id='" + str(bike_id) + "' and dealer_id='" + str(user_id) + "';"
		cur.execute(check_query)
		x = cur.fetchone()
		if x is None:
			#INSERT into quotes table 
			insert_quote_query = "INSERT INTO quotes (bike_id, dealer_id, quote, sold_to) VALUES (%s,%s,%s,%s)"
			quote_data = (bike_id, user_id, price, 0)
			cur.execute(insert_quote_query, quote_data)
			conn.commit()
			continue
		if x is not None:
			#Update the existing quote
			update_query = "UPDATE quotes SET quote='" + str(price) + "' WHERE bike_id='" + str(bike_id) + "' and dealer_id='" + str(user_id) + "';"
			cur.execute(update_query)
			conn.commit()
			continue