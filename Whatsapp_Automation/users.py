import psycopg2
import json
import re
import traceback

conn = psycopg2.connect(database="postgres", user="postgres", password="123", host="127.0.0.1", port="5432")
cur = conn.cursor()
print "Connection Established"

query = "Select * from bikes"
cur.execute(query)

rows = cur.fetchall()
data = dict()

for row in rows :
	for i in range(1,3):
		try:
			data = json.loads(row[9])
			query1 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[2] + "','" + json.dumps(data.get(str(i))) + "',0);"
			cur.execute(query1)  
			conn.commit()
		except:
			conn.rollback()
			traceback.print_exc()	