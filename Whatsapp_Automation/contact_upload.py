# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import pyautogui
import psycopg2
import json
import re
import traceback
from tqdm import tqdm

conn = psycopg2.connect(database="whatsapp_bike", user="postgres", password="credr", host="52.74.154.2", port="5432")
cur = conn.cursor()
print "----------------------------Connection Established----------------------------"

import pandas as pd

df = pd.read_csv('/home/jeet/Downloads/Whatsapp_Dealers.csv')
contact_no = df['Whatsapp No.']
Shop_Name = df['UserName']
print "------------------------------Contacts read------------------------------------"
# print contact_no, Shop_Name

class ContactUpload(unittest.TestCase):

    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://mail.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_contact_upload(self):
        flag = 0
        global contact_no,Dealer_id
        driver = self.driver
        driver.get(self.base_url + "/mail/u/2/#contact/new")
        # print len(contact_no)
        for i in tqdm(range(len(contact_no))):
            if flag == 0:
                driver.find_element_by_xpath("//input[@class='R5 Rh']").clear()
                driver.find_element_by_xpath("//input[@class='R5 Rh']").send_keys(Shop_Name[i])
                flag = 1
            elif flag == 1:
                driver.find_element_by_css_selector("span.acm > input.R5.Rh").clear()
                driver.find_element_by_css_selector("span.acm > input.R5.Rh").send_keys(Shop_Name[i])
            time.sleep(1)   
            for j in range(0,9):
                pyautogui.press('tab')
                time.sleep(0.35)
            time.sleep(1)
            print contact_no[i]
            pyautogui.typewrite(str(contact_no[i]))
            time.sleep(1)
            driver.find_element_by_xpath("//div[@class='MX']/child::div").click()
            time.sleep(1)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    # def tearDown(self):
    #     self.driver.quit()
    #     self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
