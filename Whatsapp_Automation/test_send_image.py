# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import pyautogui
import urllib
import os

def download_image(image_source):
   
    urllib.urlretrieve(image_source,"/home/jeet/CredR/Whatsapp_Automation/images/upload.jpg")
    print "image Downloaded"

def delete_file():
   
    fname = os.listdir('/home/jeet/CredR/Whatsapp_Automation/images')
    print fname
    os.remove('/home/jeet/CredR/Whatsapp_Automation/images/' + str(fname[0]))
    fname.pop(0)
    print "file deleted"

class TestSendImage(unittest.TestCase):

    sending_to = "random"
    caption = "caption"
    
    # def test_image(self):
    #     print "broadcasting_list", self.sending_to
    
    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_xpath("//span[@title='"+ self.sending_to +"']").click()
        driver.find_element_by_css_selector("button.icon.icon-clip").click()
        driver.find_element_by_css_selector("button.menu-icons-item").click()
        driver.find_element_by_xpath("(//input[@type='file'])[2]")
        pyautogui.press('enter')
        time.sleep(5)
        pyautogui.typewrite(self.caption)
        time.sleep(3)
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    # def tearDown(self):
    #     self.driver.quit()
    #     self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    TestSendImage.sending_to = "Vikas Reddy"
    TestSendImage.caption = "Caption"
    image_source = "http://static.comicvine.com/uploads/original/11114/111144096/3697258-8667104673--Mada.png"
    download_image(image_source)
    unittest.main()
    delete_file()
