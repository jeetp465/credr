# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Ctrl1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_ctrl1(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_xpath("//div[@id='pane-side']/div/div/div/div[5]/div/div/div[2]/div[2]/div/span[3]").click()
        driver.find_element_by_css_selector("button.icon.icon-clip").click()
        driver.find_element_by_css_selector("button.menu-icons-item").click()
        driver.find_element_by_xpath("(//input[@type='file'])[2]").clear()
        driver.find_element_by_xpath("(//input[@type='file'])[2]").send_keys("/home/jeet/CredR/Whatsapp_Automation/images/Screenshot from 2016-04-05 20:21:59.png")
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span/div[2]/div").click()
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span/div[3]/div").click()
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span/div[4]/div").click()
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
