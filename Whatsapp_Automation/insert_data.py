import psycopg2
import json
import re
import traceback

conn = psycopg2.connect(database="postgres", user="postgres", password="123", host="127.0.0.1", port="5432")
cur = conn.cursor()
print "Connection Established"

######################################################################################################################
############################################     SELECTING DATA FROM BIKES     #######################################
######################################################################################################################

query = "Select * from bikes where status = 0"
cur.execute(query)

rows = cur.fetchall()
data = dict()

for row in rows :
	for i in range(1,3):
		try:
			data = json.loads(row[9])
			query1 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + str(row[2]) + "','" + json.dumps(data.get(str(i))) + "',0);"
			cur.execute(query1)  
			conn.commit()
			query1 = "Update bikes set status = 2 where city = '" + str(row[2]) + "';"
			cur.execute(query1)
			conn.commit()
		except:
			conn.rollback()
			traceback.print_exc()
	#tested

while(1):

	#####################################################################################################################
	##########################################     SELECTING DATA FROM RECEIVER     #####################################
	#####################################################################################################################

	query = "Select * from receiver where status != 6"

	cur.execute(query)

	rows = cur.fetchall()

	for row in rows :
		if row[4] != 6:
			try:
				if row[4] == 3:
					query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','{\"text\" :\"The message is in wrong format\"}',0);"
					cur.execute(query3)
					conn.commit()
					query = "update receiver set status = 6 where from_name = '" + str(row[3]) + "';"
					cur.execute(query)
					conn.commit()
				elif row[4] == 4:
					query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','{\"text\":\"The bidding process has been terminated\"}',0);"
					cur.execute(query3)
					conn.commit()
					query = "update receiver set status = 6 where from_name = '" + str(row[3]) + "';"
					cur.execute(query)
					conn.commit()
				elif row[4] == 5:
					query3 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + row[3] + "','{\"text\":\"The bike does not exist\"}',0);"
					cur.execute(query3)
					conn.commit()
					query = "update receiver set status = 6 where from_name = '" + str(row[3]) + "';"
					cur.execute(query)
					conn.commit()
			except:
				conn.rollback()
				traceback.print_exc()
		else :
			continue


	#tested

	#####################################################################################################################
	###########################################     SELECTING DATA FROM QUOTES    #######################################
	#####################################################################################################################		

	query1 = "Select dealer_id, bike_id from quotes where sold_to = 1"
	cur.execute(query1)
	rows1 = cur.fetchall()
	print rows1

	for i in range(len(rows1)):
		query = "Select dealer_id from quotes where bike_id ='" + str(rows1[i][1]) +"';"
		cur.execute(query)
		rows = cur.fetchall()

		for row in rows :
			try :
				if row[0][0] == int(rows1[i][0]) :
					query2 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + str(rows1[i][0]) + "','{\"text\" :\"The bike with bike id : " + str(rows1[i][1]) + " has been sold to you.\"}',0);"
					cur.execute(query2)
					conn.commit()
				else :
					query2 = "Insert into sender (shop_name,message,sms_status) VALUES ('" + str(rows1[i][0]) + "','\"text\":\"The bike with bike id : " + str(rows1[i][1]) + " has been sold.\"}');"
					cur.execute(query2)
					conn.commit()
			except:
				conn.rollback()
				traceback.print_exc()

	query = "Update quotes set sold_to = 2 where sold_to = 1"
	cur.execute(query)
	conn.commit()

#tested