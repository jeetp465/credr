# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import pyautogui

class Test(unittest.TestCase):
    def setUp(self):
        self.profile = webdriver.FirefoxProfile('/home/jeet/.mozilla/firefox/6g4wnvg7.default')
        self.driver = webdriver.Firefox(self.profile)
        self.driver.implicitly_wait(30)
        self.base_url = "https://web.whatsapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_xpath("//span[@title='Jeet Patel']").click()
        driver.find_element_by_css_selector("button.icon.icon-clip").click()
        driver.find_element_by_css_selector("button.menu-icons-item").click()
        driver.find_element_by_xpath("(//input[@type='file'])[2]")
        pyautogui.press('enter')
        time.sleep(3)
        driver.find_element_by_xpath("//div[@id='app']/div/div[3]/div/span[2]/span/div/div[2]/span[2]/div/button").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
