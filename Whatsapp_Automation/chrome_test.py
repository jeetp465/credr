from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import os


# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options

# options = webdriver.ChromeOptions() 
# options.add_argument("user-data-dir=C:\\Path") #Path to your chrome profile
# w = webdriver.Chrome(executable_path="C:\\Users\\chromedriver.exe", chrome_options=options)
class chrome_browser(unittest.TestCase):

	def setUp(self):
		self.options = webdriver.ChromeOptions()
		self.options.add_argument('--user-data-dir=/home/jeet/.config/google-chrome/Default') #Path to your chrome profile
		self.driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", chrome_options=self.options)
		# chromedriver = "/home/jeet/chromedriver"
		# os.environ["webdriver.chrome.driver"] = chromedriver
		# self.driver = webdriver.Chrome(chromedriver)
		self.driver.implicitly_wait(30)
		self.base_url = "https://www.facebook.com"
		self.verificationErrors = []
		self.accept_next_alert = True

	def test_chrome_browser(self):
		self.driver.get(self.base_url)
		while(1):
			i = 0
			i = i + 1

	def is_element_present(self, how, what):
		try: self.driver.find_element(by=how, value=what)
		except NoSuchElementException as e: return False
		return True

	def is_alert_present(self):
		try: self.driver.switch_to_alert()
		except NoAlertPresentException as e: return False
		return True

	def close_alert_and_get_its_text(self):
		try:
			alert = self.driver.switch_to_alert()
			alert_text = alert.text
			if self.accept_next_alert:
				alert.accept()
			else:
				alert.dismiss()
			return alert_text
		finally: self.accept_next_alert = True

	# def tearDown(self):
	# 	time.sleep(3)
	# 	self.driver.quit()
	# 	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
	unittest.main()